import * as jwt from "jsonwebtoken"
import {rolePermission,permission} from "../dataModels/rolePermission"


export class AuthenticateService{
    public static Authenticate(req:any,res:any,next:any){
        try{
            let token=req.header("Authorisation");
            if(!token){
               return res.status(401).send("Acess Denied");
            }
            let decodedData=jwt.verify(token,"secretkey");//returns payload
            //console.log(decodedData);
            req.user=decodedData;
            next();
        }
        catch(err){
            console.log(err);
            res.status(401).send("Bad request,Acess denied");
        }
    }
    
    
    public static HasPermission(role:string,action:permission):Boolean{
        let permissionList = rolePermission[role];
        if(permissionList.indexOf(action)>=0||permissionList.indexOf(permission.All)>=0){
            return true;
        }else{
            return false;
        }
    }

}