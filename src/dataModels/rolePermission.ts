export enum permission{
    "AddGetUpdateDeleteMovie"=10,
    "AddGetUpdateDeleteTheatre"=20,
    "AddGetUpdateDeleteActor"=30,
    "GetShow"=40,
    "AddUpdateDeleteShow"=41,
    "CreateDeleteBooking"=50,
    "GetBooking"=51,
    "GetUser"=60,
    "All"=1000
}

export const rolePermission:any={
    "user":[permission.GetShow,permission.GetShow,permission.GetUser],
    "admin":[permission.All]
}