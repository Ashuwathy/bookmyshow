
export class ResponseModel{
    //properties
    isValid : Boolean;
    data : any;
    error : any;

    //functions
    constructor(responseStatus:Boolean,data:any,error:any){
        this.isValid = responseStatus;
        this.data = data;
        this.error = error;

    }

    public static async GetValidResponse(data:any){
        return new ResponseModel(true,data,null);

    }

    public static async GetInvalidResponse(error:any){
        return new ResponseModel(false,null,error);

    }

}