import {userModel} from "../models/userModel"
import express from "express"
import {ResponseModel} from "../dataModels/responseModel"
import bcryptClient from "bcryptjs"
import * as jwt from "jsonwebtoken"

export class UserService{

    //functions
    public static async Register(req:express.Request){
        try{
            let salt = await bcryptClient.genSalt(10);
            let hashPassword = await bcryptClient.hash(req.body.pass,salt);
            req.body.pass = hashPassword;
            let newUser = new userModel(req.body);
            await newUser.save();
            return ResponseModel.GetValidResponse(newUser);
        }
        catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }

    }

    public static async Login(req:express.Request){
        try{
            let user:any = await userModel.findOne({mobile:req.body.mobile}).exec();
            if(user==null){
                return ResponseModel.GetInvalidResponse("user doesnot exist with mobile number "+req.body.mobile);
            }else{
                let passwordMatch = await bcryptClient.compare(req.body.pass,user.pass);
                if(passwordMatch){
                    //return ResponseModel.GetValidResponse("login sucess");
                    //token generation
                    let payload={
                        "userId":user._id,
                        "username":user.name,
                        "email":user.email,
                        "role":user.role
                    }
                    let option : jwt.SignOptions ={
                        expiresIn:"12d"
                    }
                    let token = jwt.sign(payload,"secretkey",option);
                    return ResponseModel.GetValidResponse({'token':token});
                    
                }else{
                    return ResponseModel.GetInvalidResponse("pass incorrect");
                }

            }
            
        }catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
        
    }

    public static async GetAllUsers(){
        try{
            let allUsers = await userModel.find({}).exec();//.exec will return promise obj
            return ResponseModel.GetValidResponse(allUsers);
            
        }
        catch(err){
            console.log(err);
            return ResponseModel.GetInvalidResponse(err);

        }
    }

}