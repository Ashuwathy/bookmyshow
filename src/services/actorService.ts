import {actorModel} from "../models/actorModel"
import express from "express"
import {ResponseModel} from "../dataModels/responseModel"

export class ActorService{

    //functions
    public static async CreateActor(req:express.Request){
        try{
            let newActor = new actorModel(req.body);
            await newActor.save();
            return ResponseModel.GetValidResponse(newActor);
            
        }
        catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }

    }

    public static async GetAllActors(){
        try{
            let allActors = await actorModel.find({}).exec();//.exec will return promise obj
            return ResponseModel.GetValidResponse(allActors);
            
        }
        catch(err){
            console.log(err);
            return ResponseModel.GetInvalidResponse(err);

        }
    }

    /*public static async UpdateActor(req:express.Request){
        try{
            let actor : any = await actorModel.findById(req.params.actorId).exec();
            actor.name=req.body.name;
            await actor.save();
            return ResponseModel.GetValidResponse(actor);
        }
        catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
    }*/

    public static async UpdateActor(req:express.Request){
        try{
            let options = {new:true,upsert:true}
            let actor = await actorModel.findByIdAndUpdate(req.params.actorId,req.body,options);
            return ResponseModel.GetValidResponse(actor);
        }catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
        
    }

    public static async DeleteActor(req:express.Request){
        try{
            await actorModel.findByIdAndDelete(req.params.actorId);
            return ResponseModel.GetValidResponse("deleted");
        }catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
    }

}