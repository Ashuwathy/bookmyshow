import {bookingModel} from "../models/bookingModel"
import express from "express"
import {ResponseModel} from "../dataModels/responseModel"

export class BookingService{

    //functions
    public static async CreateBooking(req:express.Request){
        try{
            let newBooking = new bookingModel(req.body);
            await newBooking.save();
            return ResponseModel.GetValidResponse(newBooking);
            
        }
        catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }

    }

    public static async GetAllBookings(){
        try{
            let allBookings = await bookingModel.find({}).populate([//depopulate 
                {
                    path:"user",select:"name role"
                },
                {
                    path:"show",select:"showDate timing movie theatre seats",
                    populate:[
                        {
                            path:"movie",select:"name language duration cast",
                            populate:{path:"cast",select:"name"}
                        },
                        {
                            path:"theatre",select:"name address"
                        }
                    ] 
                }
            ]).exec();//.exec will return promise obj
            return ResponseModel.GetValidResponse(allBookings);
            
        }
        catch(err){
            console.log(err);
            return ResponseModel.GetInvalidResponse(err);

        }
    }

    public static async GetBookingsByUser(req:express.Request){
        try{
            let BookingsByUser = await bookingModel.find({user:req.params.userId}).populate([
                {
                    path:"show",select:"showDate movie theatre timing",
                    populate:[
                        {
                            path:"movie",select:"name language duration cast",
                            populate:{path:"cast",select:"name"}
                        },
                        {
                            path:"theatre",select:"name address"
                        }
                    ]
                },
                {
                    path:"user",select:"name"
                }
            ]).exec();
            return ResponseModel.GetValidResponse(BookingsByUser);
        }
        catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
    }

    public static async DeleteBooking(req:express.Request){
        try{
            await bookingModel.findByIdAndDelete(req.params.bookingId);
            return ResponseModel.GetValidResponse("deleted");
        }catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
    }
   

}