import {showModel} from "../models/showModel"
import express from "express"
import {ResponseModel} from "../dataModels/responseModel"

export class ShowService{

    //functions
    public static async CreateShow(req:express.Request){
        try{
            let newShow = new showModel(req.body);
            await newShow.save();
            return ResponseModel.GetValidResponse(newShow);
            
        }
        catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }

    }

    public static async GetAllShows(){
        try{
            let allShows = await showModel.find({}).populate([
                {
                    path:"movie",select:"name language duration cast",
                    populate:{path:"cast",select:"name"}
                },
                {
                    path:"theatre",select:"name address"
                }    
            
            ]).exec();//.exec will return promise obj
            return ResponseModel.GetValidResponse(allShows);
            
        }
        catch(err){
            console.log(err);
            return ResponseModel.GetInvalidResponse(err);

        }
    }

    public static async GetShowsByTime(req:express.Request){
        try{
            let ShowsByTime = await showModel.find({timing:{start: parseInt(req.query.start),end: parseInt(req.query.end)}}).exec();//.exec will return promise obj
            if(ShowsByTime){
                return ResponseModel.GetValidResponse(ShowsByTime);
            }else{
                return ResponseModel.GetInvalidResponse("shows not available @ "+req.query.start+req.query.end);
            }
            
        }
        catch(err){
            console.log(err);
            return ResponseModel.GetInvalidResponse(err);

        }
    }

    public static async GetShowsByTheatre(req:express.Request){
        try{
            let allShows : any[] = await showModel.find({}).populate([
                {
                    path:"movie",select:"name language duration cast",
                    populate:{path:"cast",select:"name"}
                },
                {
                    path:"theatre",select:"name address"
                }    
            
            ]).exec();//.exec will return promise obj
            if(allShows.length > 0){
                let ShowsByTheatre : any[]=[];
                ShowsByTheatre=allShows.filter((item)=>{
                    if(item.theatre.name==req.query.theatreName){
                        ShowsByTheatre.push(item);
                    }
                });

                return ResponseModel.GetValidResponse(ShowsByTheatre);
            }else{
                return ResponseModel.GetInvalidResponse("shows not available in "+req.params.theatreId);
            }
            
        }
        catch(err){
            console.log(err);
            return ResponseModel.GetInvalidResponse(err);

        }
    
    }

    public static async GetShowsByDate(req:express.Request){
        try{
            let ShowsByDate = await showModel.find({showDate:req.query.showDate}).populate([
                {
                    path:"movie",select:"name language duration cast",
                    populate:{path:"cast",select:"name"}
                },
                {
                    path:"theatre",select:"name address"
                }
            ]).exec();
            if(ShowsByDate){
                return ResponseModel.GetValidResponse(ShowsByDate);
            }else{
                return ResponseModel.GetInvalidResponse("shows not available @ "+req.query.showDate);
            }
        }
        catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }

    }

    public static async GetShowsByMovie(req:express.Request){
        try{
            let allShows : any[] = await showModel.find({}).populate([
                {
                    path:"movie",select:"name language duration cast",
                    populate:{path:"cast",select:"name"}
                },
                {
                    path:"theatre",select:"name address"
                }    
            
            ]).exec();//.exec will return promise obj
            if(allShows.length > 0){
                let ShowsByMovie : any[]=[];
                ShowsByMovie = allShows.filter((item)=>{
                    if(item.movie.name==req.query.movieName){
                        ShowsByMovie.push(item);
                    }
                });

                return ResponseModel.GetValidResponse(ShowsByMovie);
            }else{
                return ResponseModel.GetInvalidResponse("shows not available in "+req.params.theatreId);
            }
            
        }
        catch(err){
            console.log(err);
            return ResponseModel.GetInvalidResponse(err);

        }
    }

    public static async UpdateShow(req:express.Request){
       try{
           let options = {new:true,upsert:true}
           let show = await showModel.findByIdAndUpdate(req.params.showId,req.body,options);
           return ResponseModel.GetValidResponse(show);
       }catch(err){
           return ResponseModel.GetInvalidResponse(err);
       }
    }

    public static async DeleteShow(req:express.Request){
        try{
            await showModel.findByIdAndDelete(req.params.showId);
            return ResponseModel.GetValidResponse("deleted");
        }catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
    }

}