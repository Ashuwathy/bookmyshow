import {theatreModel} from "../models/theatreModel"
import express from "express"
import {ResponseModel} from "../dataModels/responseModel"

export class TheatreService{

    //functions
    public static async CreateTheatre(req:express.Request){
        try{
            let newTheatre = new theatreModel(req.body);
            await newTheatre.save();
            return ResponseModel.GetValidResponse(newTheatre);
            
        }
        catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }

    }

    public static async GetAllTheatres(){
        try{
            let allTheatres = await theatreModel.find({}).exec();//.exec will return promise obj
            return ResponseModel.GetValidResponse(allTheatres);
            
        }
        catch(err){
            console.log(err);
            return ResponseModel.GetInvalidResponse(err);

        }
    }

    public static async UpdateTheatre(req:express.Request){
        try{
            let options = {new:true,upsert:true}
            let theatre = await theatreModel.findByIdAndUpdate(req.params.theatreId,req.body,options);
            return ResponseModel.GetValidResponse(theatre);
        }catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
    }

    public static async DeleteTheatre(req:express.Request){
        try{
            await theatreModel.findByIdAndDelete(req.params.theatreId);
            return ResponseModel.GetValidResponse("deleted");
        }catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
    }

    
}