import {movieModel} from "../models/movieModel"
import express from "express"
import {ResponseModel} from "../dataModels/responseModel"

export class MovieService{

    //functions
    public static async CreateMovie(req:express.Request){
        try{
            let newMovie = new movieModel(req.body);
            await newMovie.save();
            return ResponseModel.GetValidResponse(newMovie);
            
        }
        catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }

    }

    public static async GetAllMovies(){
        try{
            let allMovies = await movieModel.find({}).populate("cast").exec();//.exec will return promise obj
            return ResponseModel.GetValidResponse(allMovies);
            
        }
        catch(err){
            console.log(err);
            return ResponseModel.GetInvalidResponse(err);

        }
    }

    public static async UpdateMovie(req:express.Request){
        try{
            let options = {new:true,upsert:true}
            let movie = await movieModel.findByIdAndUpdate(req.params.movieId,req.body,options);
            return ResponseModel.GetValidResponse(movie);
        }catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
    }

    public static async DeleteMovie(req:express.Request){
        try{
            await movieModel.findByIdAndDelete(req.params.movieId);
            return ResponseModel.GetValidResponse("deleted");
        }catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
    }

}