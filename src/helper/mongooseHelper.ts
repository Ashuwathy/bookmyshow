import {inventoryModel} from "../models/inventryModel"
import {MongoErrorHandler} from "../helper/mongoError"

export class MongooseHelper{
    public static async InsertOne(req:any,res:any){
        let data = {
            item:"jolly",
            qty:15,
            tags:["cotton"],
            size:{h:28,w:35,uom:"cm"}
        };
        try{
            let item = new inventoryModel(data);
            await item.save();
            res.json(item);
        }catch(err){
            //res.json(err);
            let response:any = MongoErrorHandler.handleError(err)
            res.json(response);
        }
    }

    public static async InsertMany(req:any,res:any){
        let data = [
            {
                item:"canvas",
                qty:12,
                tags:["cotton"],
                size:{h:28,w:35,uom:"cm"}
            },
            {
                item:"journal",
                qty:67,
                tags:["black"],
                size:{h:23,w:67,uom:"cm"}
            },
            {
                item:"mat",
                qty:15,
                tags:["red"],
                size:{h:17,w:89,uom:"cm"}
            }
        ]
        try{
            let result = await inventoryModel.insertMany(data);
            return res.json(result);
        }catch(err){
            res.json(err);
        }
    }

    public static async FindItem(req:any,res:any){
        let item = await inventoryModel.find({item:"canvas"});
        return res.json(item);
    }

    public static async FindAllItem(req:any,res:any){
        let allitems = await inventoryModel.find({});
        return res.json(allitems);
    }

    public static async Update(req:any,res:any){
        try{
            let option = {runValidator:true}
            let result = await inventoryModel.update({item:"mat"},{qty:5},option);
            res.json(result);
        }catch(err){
            return res.json(err);
        }
    }

    public static async UpdateMany(req:any,res:any){
        try{
            let option = {runValidator:true,multi:true}
            let result = await inventoryModel.update({qty:{$gte:20}},{tags:["item"]},option);
            res.json(result);
        }catch(err){
            return res.json(err);
        }
    }

    public static async Delete(req:any,res:any){

    }
}