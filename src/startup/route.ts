import express from "express"
import { movieRoutes } from "../routes/movieRoutes"
import { actorRoutes } from "../routes/actorRoutes"
import {bookingRoutes} from "../routes/bookingRoutes"
import {showRoutes} from "../routes/showRoutes"
import {theatreRoutes} from "../routes/theatreRoutes"
import {userRoutes} from "../routes/userRoutes"
import {UserController} from "../controller/userController"
import { AuthenticateService } from "../middleware/authenticate"
import {MongooseHelper} from "../helper/mongooseHelper"


export class Route{

    //functions
    constructor(){}

    public static configRoutes(app:express.Application){
        app.post("/api/login",UserController.Login);
        app.post("/api/register",UserController.Register);
        app.use(AuthenticateService.Authenticate);
        app.use("/api/movie",movieRoutes);//end point,action to be performed
        app.use("/api/actor",actorRoutes);
        app.use("/api/booking",bookingRoutes);
        app.use("/api/show",showRoutes);
        app.use("/api/theatre",theatreRoutes);
        app.use("/api/user",userRoutes);

    }


        /*
        //mongoose methods
        app.post("/api/inventory/insertOne",MongooseHelper.InsertOne);
        app.post("/api/inventory/insertMany",MongooseHelper.InsertMany);
        app.get("/api/inventory/getOneItem",MongooseHelper.FindItem);
        app.get("/api/inventory/getAllItem",MongooseHelper.FindAllItem);
        app.put("/api/inventory/updateItem",MongooseHelper.Update);
        app.put("/api/inventory/updateMany",MongooseHelper.UpdateMany);*/
}