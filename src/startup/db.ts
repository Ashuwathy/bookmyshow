import mongoose from "mongoose"

export class DB{
    //properties
    static mongoUrl : string = "mongodb://localhost:27017/ShowDB"//mongodb server url and mydb is database name and it is connectivity string
    
    //functions
    constructor(){}

    public static ConnectMongoDb(){
        mongoose.connect(this.mongoUrl)//method to connect to database
           .then(()=>{
               console.log("ShowDB connected");
            })

           .catch((err)=>{
               console.log(err);
            });
           
    }

}