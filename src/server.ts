import express from "express"
import bodyParser from "body-parser"
import {Route} from "./startup/route"
import { DB } from "./startup/db";

class BookMyShow{
    //properties
    app : express.Application;

    //functions
    constructor(){
        this.app=express();
        this.app.listen(3000,'localhost',()=>{
            console.log("server started");
        });
        this.configBodyParser();
        Route.configRoutes(this.app);
        DB.ConnectMongoDb();
    }

    private configBodyParser(){
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended:true}));
    }

}

let book = new BookMyShow();