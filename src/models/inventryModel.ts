import mongoose from "mongoose"

let inventorySchema = new mongoose.Schema({
    item : {type:String,required:true},
    qty : {
        type:Number,
        validate:
        {
            validator:function(v:any)
            {
                return v>10
            },
            message : "quantity should be greater than 10"
        }
    },
    size:{
        h:Number,
        w:Number,
        uom:String
    },
    tags:[String],
    status:String

});

export const inventoryModel = mongoose.model("inventory",inventorySchema)