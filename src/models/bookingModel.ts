import mongoose from "mongoose"

let bookingSchema = new mongoose.Schema({
    show : {type:mongoose.Schema.Types.ObjectId,ref:"show"},
    user : {type:mongoose.Schema.Types.ObjectId,ref:"user"},
    date : {type:Date,default:Date.now},
    status : {type:String,enum:['pending','completed','failed'],default:'completed'},

});

export const bookingModel = mongoose.model("booking",bookingSchema);//register collection in database