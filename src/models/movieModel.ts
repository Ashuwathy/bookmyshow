import mongoose from "mongoose"

let movieSchema = new mongoose.Schema({
    name : {type:String,required:true,unique:true},
    language : {type:String,enum:['hindi','tamil','english'],default:'tamil'},
    duration : {type:Number},
    cast : [{type:mongoose.Schema.Types.ObjectId,ref:"actor"}],
    active : {type:Boolean,default:false},
    createdDate : {type:Date,default:Date.now},
    updatedDate : {type:Date,default:Date.now},

});

export const movieModel = mongoose.model("movie",movieSchema);