import mongoose from "mongoose"
import {addressSchema} from "./common/addressSchema"

let theatreSchema = new mongoose.Schema({
    name : {type:String,required:true},
    address : addressSchema
});

export const theatreModel = mongoose.model("theatre",theatreSchema);