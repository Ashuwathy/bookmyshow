import mongoose from "mongoose"

let showSchema = new mongoose.Schema({
    showDate : {type:Date,default:Date.now},
    timing : {start:Number,end:Number},
    movie : [{type:mongoose.Schema.Types.ObjectId,ref:"movie"},{type:mongoose.Schema.Types.ObjectId,ref:"theatre"}],
    price : {type:Number,required:true},
    seats : {type:Number,default:25},
    theatre : {type:mongoose.Schema.Types.ObjectId,ref:"theatre"}

});

export const showModel = mongoose.model("show",showSchema);