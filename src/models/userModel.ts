import mongoose from "mongoose"

let userSchema = new mongoose.Schema({
    name : {type:String,required:true},
    mobile : {type:String,required:true,unique:true},
    email : {type:String,required:true},
    pass : {type:String,required:true},
    role : {type:String,enum:["admin","user"],default:"user"}
});

export const userModel = mongoose.model("user",userSchema);