import mongoose from "mongoose"

export const addressSchema = new mongoose.Schema({
    "landMark" : String,
    "locality" : String,
    "city" : {type:String,requried:true},
    "postalCode" : String

});