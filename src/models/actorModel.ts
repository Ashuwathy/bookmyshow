import mongoose from "mongoose"

let actorSchema = new mongoose.Schema({
    name : {type:String,required:true,unique:true},
    gender : {type:String,enum:['male','female']},

});

export const actorModel = mongoose.model("actor",actorSchema);