import express from "express"
import {TheatreService} from "../services/theatreService"
import {AuthenticateService} from "../middleware/authenticate"
import {permission} from "../dataModels/rolePermission"

export class TheatreController{
    
    //functions
    public static async CreateTheatre(req:express.Request,res:express.Response){
        let response = await TheatreService.CreateTheatre(req);
        return res.json(response);
    }

    public static async GetAllTheatres(req:express.Request,res:express.Response){
        let response = await TheatreService.GetAllTheatres();
        return res.json(response);
    }

    public static async UpdateTheatre(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddGetUpdateDeleteMovie);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await TheatreService.UpdateTheatre(req);
        return res.json(response);
    }

    public static async DeleteTheatre(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddGetUpdateDeleteMovie);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await TheatreService.DeleteTheatre(req);
        return res.json(response);
    }

}