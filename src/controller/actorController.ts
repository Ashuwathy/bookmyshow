import express from "express"
import {ActorService} from "../services/actorService"
import {AuthenticateService} from "../middleware/authenticate"
import { permission } from "../dataModels/rolePermission";

export class ActorController{
    
    //functions
    public static async CreateActor(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddGetUpdateDeleteActor);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ActorService.CreateActor(req);
        return res.json(response);
        
    }

    public static async GetAllActors(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddGetUpdateDeleteActor);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ActorService.GetAllActors();
        return res.json(response);
    }

    public static async UpdateActor(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddGetUpdateDeleteActor);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ActorService.UpdateActor(req);
        return res.json(response);
    }

    public static async DeleteActor(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddGetUpdateDeleteActor);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ActorService.DeleteActor(req);
        return res.json(response);
    }


}