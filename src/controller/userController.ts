import express from "express"
import {UserService} from "../services/userService"
import {AuthenticateService} from "../middleware/authenticate"
import {permission} from "../dataModels/rolePermission"

export class UserController{
    
    //functions
    public static async GetAllUsers(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.GetUser);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await UserService.GetAllUsers();
        return res.json(response);
    }

    public static async Login(req:express.Request,res:express.Response){
        let response = await UserService.Login(req);
        return res.json(response);

    }

    public static async Register(req:express.Request,res:express.Response){
        let response = await UserService.Register(req);
        return res.json(response);
        
    }


}