import express from "express"
import {MovieService} from "../services/movieService"
import {AuthenticateService} from "../middleware/authenticate"
import {permission} from "../dataModels/rolePermission"

export class MovieController{
    
    //functions
    public static async CreateMovie(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddGetUpdateDeleteMovie);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await MovieService.CreateMovie(req);
        return res.json(response);
    }

    public static async GetAllMovies(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddGetUpdateDeleteMovie);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await MovieService.GetAllMovies();
        return res.json(response);
    }

    public static async UpdateMovie(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddGetUpdateDeleteMovie);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await MovieService.UpdateMovie(req);
        return res.json(response);
    }

    public static async DeleteMovie(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddGetUpdateDeleteMovie);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await MovieService.DeleteMovie(req);
        return res.json(response);
    }

}