import express from "express"
import {BookingService} from "../services/bookingService"
import {AuthenticateService} from "../middleware/authenticate"
import {permission} from "../dataModels/rolePermission"

export class BookingController{
    
    //functions
    public static async CreateBooking(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.CreateDeleteBooking);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await BookingService.CreateBooking(req);
        return res.json(response);
    }

    public static async GetAllBookings(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.GetBooking);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await BookingService.GetAllBookings();
        return res.json(response);
    }

    public static async GetBookingsByUser(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.GetBooking);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await BookingService.GetBookingsByUser(req);
        return res.json(response);
    }

    public static async DeleteBooking(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.CreateDeleteBooking);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await BookingService.DeleteBooking(req);
        return res.json(response);
    }

}