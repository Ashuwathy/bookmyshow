import express from "express"
import {ShowService} from "../services/showService"
import {AuthenticateService} from "../middleware/authenticate"
import {permission} from "../dataModels/rolePermission"

export class ShowController{
    
    //functions
    public static async CreateShow(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.GetShow);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ShowService.CreateShow(req);
        return res.json(response);
    }

    public static async GetAllShows(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.GetShow);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ShowService.GetAllShows();
        return res.json(response);
    }

    public static async GetShowsByTime(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.GetShow);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ShowService.GetShowsByTime(req);
        return res.json(response);
    }

    public static async GetShowsByTheatre(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.GetShow);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ShowService.GetShowsByTheatre(req);
        return res.json(response);
    }

    public static async GetShowsByDate(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.GetShow);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ShowService.GetShowsByDate(req);
        return res.json(response);
    }

    public static async GetShowsByMovie(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.GetShow);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ShowService.GetShowsByMovie(req);
        return res.json(response);
    }

    public static async UpdateShow(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.AddUpdateDeleteShow);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ShowService.UpdateShow(req);
        return res.json(response);

    }

    public static async DeleteShow(req:any,res:express.Response){
        let access = AuthenticateService.HasPermission(req.user.role,permission.GetShow);
        if(!access){
            return res.status(401).send("you dont have permission to perform action");
        }
        let response = await ShowService.DeleteShow(req);
        return res.json(response);
    }


}