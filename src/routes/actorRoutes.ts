import {Router} from "express"
import {ActorController} from "../controller/actorController"

export const actorRoutes : Router = Router();
actorRoutes.post("/create",ActorController.CreateActor);
actorRoutes.get("/getAll",ActorController.GetAllActors);
actorRoutes.put("/update/:actorId",ActorController.UpdateActor);
actorRoutes.delete("/delete/:actorId",ActorController.DeleteActor);
