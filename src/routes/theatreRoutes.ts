import {Router} from "express"
import {TheatreController} from "../controller/theatreController"

export const theatreRoutes : Router = Router();// router  
theatreRoutes.post("/create",TheatreController.CreateTheatre);
theatreRoutes.get("/getAll",TheatreController.GetAllTheatres);
theatreRoutes.put("/update/:theatreId",TheatreController.UpdateTheatre);
theatreRoutes.delete("/delete/:theatreId",TheatreController.DeleteTheatre);