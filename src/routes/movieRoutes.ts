import {Router} from "express"
import {MovieController} from "../controller/movieController"

export const movieRoutes : Router = Router();// router  
movieRoutes.post("/create",MovieController.CreateMovie);
movieRoutes.get("/getAll",MovieController.GetAllMovies);
movieRoutes.put("/update/:movieId",MovieController.UpdateMovie);
movieRoutes.delete("/delete/:movieId",MovieController.DeleteMovie);