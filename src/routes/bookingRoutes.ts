import {Router} from "express"
import {BookingController} from "../controller/bookingController"

export const bookingRoutes : Router = Router();// router  
bookingRoutes.post("/create",BookingController.CreateBooking);
bookingRoutes.get("/getAll",BookingController.GetAllBookings);
bookingRoutes.post("/getByUser/:userId",BookingController.GetBookingsByUser);
bookingRoutes.delete("/delete/:bookingId",BookingController.DeleteBooking);