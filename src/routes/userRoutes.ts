import {Router} from "express"
import {UserController} from "../controller/userController"
import {AuthenticateService} from "../middleware/authenticate"

export const userRoutes : Router = Router();// router  
userRoutes.get("/getAll",UserController.GetAllUsers);
