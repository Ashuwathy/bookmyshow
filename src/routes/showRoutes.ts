import {Router} from "express"
import {ShowController} from "../controller/showController"

export const showRoutes : Router = Router();// router  
showRoutes.post("/create",ShowController.CreateShow);
showRoutes.get("/getAll",ShowController.GetAllShows);
showRoutes.get("/getByTime",ShowController.GetShowsByTime);
showRoutes.get("/getByTheatre",ShowController.GetShowsByTheatre);
showRoutes.get("/getByDate",ShowController.GetShowsByDate);
showRoutes.get("/getByMovie",ShowController.GetShowsByMovie);
showRoutes.put("/update/:showId",ShowController.UpdateShow);
showRoutes.delete("/delete/:showId",ShowController.DeleteShow);