"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var route_1 = require("./startup/route");
var db_1 = require("./startup/db");
var BookMyShow = /** @class */ (function () {
    //functions
    function BookMyShow() {
        this.app = express_1.default();
        this.app.listen(3000, 'localhost', function () {
            console.log("server started");
        });
        this.configBodyParser();
        route_1.Route.configRoutes(this.app);
        db_1.DB.ConnectMongoDb();
    }
    BookMyShow.prototype.configBodyParser = function () {
        this.app.use(body_parser_1.default.json());
        this.app.use(body_parser_1.default.urlencoded({ extended: true }));
    };
    return BookMyShow;
}());
var book = new BookMyShow();
