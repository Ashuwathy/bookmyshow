"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var bookingController_1 = require("../controller/bookingController");
exports.bookingRoutes = express_1.Router(); // router  
exports.bookingRoutes.post("/create", bookingController_1.BookingController.CreateBooking);
exports.bookingRoutes.get("/getAll", bookingController_1.BookingController.GetAllBookings);
exports.bookingRoutes.post("/getByUser/:userId", bookingController_1.BookingController.GetBookingsByUser);
exports.bookingRoutes.delete("/delete/:bookingId", bookingController_1.BookingController.DeleteBooking);
