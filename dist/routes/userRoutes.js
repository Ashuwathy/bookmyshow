"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var userController_1 = require("../controller/userController");
exports.userRoutes = express_1.Router(); // router  
exports.userRoutes.get("/getAll", userController_1.UserController.GetAllUsers);
