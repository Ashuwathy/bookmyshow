"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var theatreController_1 = require("../controller/theatreController");
exports.theatreRoutes = express_1.Router(); // router  
exports.theatreRoutes.post("/create", theatreController_1.TheatreController.CreateTheatre);
exports.theatreRoutes.get("/getAll", theatreController_1.TheatreController.GetAllTheatres);
exports.theatreRoutes.put("/update/:theatreId", theatreController_1.TheatreController.UpdateTheatre);
exports.theatreRoutes.delete("/delete/:theatreId", theatreController_1.TheatreController.DeleteTheatre);
