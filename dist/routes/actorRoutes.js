"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var actorController_1 = require("../controller/actorController");
exports.actorRoutes = express_1.Router();
exports.actorRoutes.post("/create", actorController_1.ActorController.CreateActor);
exports.actorRoutes.get("/getAll", actorController_1.ActorController.GetAllActors);
exports.actorRoutes.put("/update/:actorId", actorController_1.ActorController.UpdateActor);
exports.actorRoutes.delete("/delete/:actorId", actorController_1.ActorController.DeleteActor);
