"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var showController_1 = require("../controller/showController");
exports.showRoutes = express_1.Router(); // router  
exports.showRoutes.post("/create", showController_1.ShowController.CreateShow);
exports.showRoutes.get("/getAll", showController_1.ShowController.GetAllShows);
exports.showRoutes.get("/getByTime", showController_1.ShowController.GetShowsByTime);
exports.showRoutes.get("/getByTheatre", showController_1.ShowController.GetShowsByTheatre);
exports.showRoutes.get("/getByDate", showController_1.ShowController.GetShowsByDate);
exports.showRoutes.get("/getByMovie", showController_1.ShowController.GetShowsByMovie);
exports.showRoutes.put("/update/:showId", showController_1.ShowController.UpdateShow);
exports.showRoutes.delete("/delete/:showId", showController_1.ShowController.DeleteShow);
