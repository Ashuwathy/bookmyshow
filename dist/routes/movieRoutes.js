"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var movieController_1 = require("../controller/movieController");
exports.movieRoutes = express_1.Router(); // router  
exports.movieRoutes.post("/create", movieController_1.MovieController.CreateMovie);
exports.movieRoutes.get("/getAll", movieController_1.MovieController.GetAllMovies);
exports.movieRoutes.put("/update/:movieId", movieController_1.MovieController.UpdateMovie);
exports.movieRoutes.delete("/delete/:movieId", movieController_1.MovieController.DeleteMovie);
