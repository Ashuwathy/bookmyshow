"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var jwt = __importStar(require("jsonwebtoken"));
var rolePermission_1 = require("../dataModels/rolePermission");
var AuthenticateService = /** @class */ (function () {
    function AuthenticateService() {
    }
    AuthenticateService.Authenticate = function (req, res, next) {
        try {
            var token = req.header("Authorisation");
            if (!token) {
                return res.status(401).send("Acess Denied");
            }
            var decodedData = jwt.verify(token, "secretkey"); //returns payload
            //console.log(decodedData);
            req.user = decodedData;
            next();
        }
        catch (err) {
            console.log(err);
            res.status(401).send("Bad request,Acess denied");
        }
    };
    AuthenticateService.HasPermission = function (role, action) {
        var permissionList = rolePermission_1.rolePermission[role];
        if (permissionList.indexOf(action) >= 0 || permissionList.indexOf(rolePermission_1.permission.All) >= 0) {
            return true;
        }
        else {
            return false;
        }
    };
    return AuthenticateService;
}());
exports.AuthenticateService = AuthenticateService;
