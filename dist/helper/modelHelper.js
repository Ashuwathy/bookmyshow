"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ModelHelper = /** @class */ (function () {
    function ModelHelper() {
    }
    ModelHelper.emailValidator = function (email) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    };
    Object.defineProperty(ModelHelper, "invalidEmailMsg", {
        get: function () {
            return '{VALUE} is not a valid email id!';
        },
        enumerable: true,
        configurable: true
    });
    ModelHelper.mobileNumberValidator = function (mobile) {
        return /^\d{10}$/.test(mobile);
    };
    Object.defineProperty(ModelHelper, "invalidMobileNumberMsg", {
        get: function () {
            return '{VALUE} is not a valid mobile number!';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModelHelper, "secretKey", {
        get: function () {
            return "secret key";
        },
        enumerable: true,
        configurable: true
    });
    ModelHelper.updateFields = function (source, destination) {
        for (var prop in destination) {
            if (prop == "_id" || prop == "__v" || prop == "createdAt" || prop == "seqCode") {
                continue;
            }
            if (destination.hasOwnProperty(prop)) {
                source[prop] = destination[prop];
            }
        }
        return source;
    };
    return ModelHelper;
}());
exports.ModelHelper = ModelHelper;
