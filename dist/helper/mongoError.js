"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var responseModel_1 = require("../dataModels/responseModel");
var MongoErrorHandler = /** @class */ (function () {
    function MongoErrorHandler() {
    }
    MongoErrorHandler.handleError = function (err) {
        // Log error.
        // logger.error(err.message);
        console.error(err);
        console.error(err.message);
        // Handle error.
        return responseModel_1.ResponseModel.GetInvalidResponse(this.getError(err));
    };
    MongoErrorHandler.getError = function (err) {
        if (err && err.name && err.name == 'ValidatorError') {
            return [MongoErrorHandler.formatValidationErrorMessage(err.message)];
        }
        if (err && err.name && err.name == 'MongoError') {
            return [MongoErrorHandler.formatMongoErrorMessage(err)];
        }
        if (err && err.name && err.name == 'CastError') {
            return [MongoErrorHandler.formatCastErrorMessage(err)];
        }
        return ["Something went wrong! " + err.message];
    };
    MongoErrorHandler.formatValidationErrorMessage = function (message) {
        message = message.replace(/Path/g, '');
        message = message.replace(/`/g, '');
        //message = message.replace(/(/g, '');
        message = message.replace(/[.*+?^${}()|[\]\\]/g, '');
        //message = message.replace("Path", '');
        return message;
    };
    MongoErrorHandler.formatMongoErrorMessage = function (err) {
        var index;
        var data = {
            name: err.name,
            message: err.message,
            code: err.code
        };
        switch (err.code) {
            case 11000:
                //data.index = err.message ? err.message.split('$', 2)[1].split(' ', 2)[0].split('_', 2)[0] : "";
                index = err.message ? err.message.split("index")[1] : "";
                data.message = 'Duplicate key error.';
                break;
        }
        return data.message + (" Require unique value for field: " + index);
    };
    MongoErrorHandler.formatCastErrorMessage = function (err) {
        Object.keys(err).forEach(function (field) {
            console.log(field + " => " + err[field]);
        });
        return "Field: " + err.path + "'s value: " + err.value + " is invalid! " + err.value + " cann't be casted to " + err.kind;
    };
    return MongoErrorHandler;
}());
exports.MongoErrorHandler = MongoErrorHandler;
