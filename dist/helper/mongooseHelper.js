"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var inventryModel_1 = require("../models/inventryModel");
var mongoError_1 = require("../helper/mongoError");
var MongooseHelper = /** @class */ (function () {
    function MongooseHelper() {
    }
    MongooseHelper.InsertOne = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var data, item, err_1, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            item: "jolly",
                            qty: 15,
                            tags: ["cotton"],
                            size: { h: 28, w: 35, uom: "cm" }
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        item = new inventryModel_1.inventoryModel(data);
                        return [4 /*yield*/, item.save()];
                    case 2:
                        _a.sent();
                        res.json(item);
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        response = mongoError_1.MongoErrorHandler.handleError(err_1);
                        res.json(response);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MongooseHelper.InsertMany = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var data, result, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = [
                            {
                                item: "canvas",
                                qty: 12,
                                tags: ["cotton"],
                                size: { h: 28, w: 35, uom: "cm" }
                            },
                            {
                                item: "journal",
                                qty: 67,
                                tags: ["black"],
                                size: { h: 23, w: 67, uom: "cm" }
                            },
                            {
                                item: "mat",
                                qty: 15,
                                tags: ["red"],
                                size: { h: 17, w: 89, uom: "cm" }
                            }
                        ];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, inventryModel_1.inventoryModel.insertMany(data)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, res.json(result)];
                    case 3:
                        err_2 = _a.sent();
                        res.json(err_2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MongooseHelper.FindItem = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var item;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, inventryModel_1.inventoryModel.find({ item: "canvas" })];
                    case 1:
                        item = _a.sent();
                        return [2 /*return*/, res.json(item)];
                }
            });
        });
    };
    MongooseHelper.FindAllItem = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var allitems;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, inventryModel_1.inventoryModel.find({})];
                    case 1:
                        allitems = _a.sent();
                        return [2 /*return*/, res.json(allitems)];
                }
            });
        });
    };
    MongooseHelper.Update = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var option, result, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        option = { runValidator: true };
                        return [4 /*yield*/, inventryModel_1.inventoryModel.update({ item: "mat" }, { qty: 5 }, option)];
                    case 1:
                        result = _a.sent();
                        res.json(result);
                        return [3 /*break*/, 3];
                    case 2:
                        err_3 = _a.sent();
                        return [2 /*return*/, res.json(err_3)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MongooseHelper.UpdateMany = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var option, result, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        option = { runValidator: true, multi: true };
                        return [4 /*yield*/, inventryModel_1.inventoryModel.update({ qty: { $gte: 20 } }, { tags: ["item"] }, option)];
                    case 1:
                        result = _a.sent();
                        res.json(result);
                        return [3 /*break*/, 3];
                    case 2:
                        err_4 = _a.sent();
                        return [2 /*return*/, res.json(err_4)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MongooseHelper.Delete = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    return MongooseHelper;
}());
exports.MongooseHelper = MongooseHelper;
