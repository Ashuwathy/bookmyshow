"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var actorModel_1 = require("../models/actorModel");
var responseModel_1 = require("../dataModels/responseModel");
var ActorService = /** @class */ (function () {
    function ActorService() {
    }
    //functions
    ActorService.CreateActor = function (req) {
        return __awaiter(this, void 0, void 0, function () {
            var newActor, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        newActor = new actorModel_1.actorModel(req.body);
                        return [4 /*yield*/, newActor.save()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse(newActor)];
                    case 2:
                        err_1 = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_1)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ActorService.GetAllActors = function () {
        return __awaiter(this, void 0, void 0, function () {
            var allActors, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, actorModel_1.actorModel.find({}).exec()];
                    case 1:
                        allActors = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse(allActors)];
                    case 2:
                        err_2 = _a.sent();
                        console.log(err_2);
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_2)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /*public static async UpdateActor(req:express.Request){
        try{
            let actor : any = await actorModel.findById(req.params.actorId).exec();
            actor.name=req.body.name;
            await actor.save();
            return ResponseModel.GetValidResponse(actor);
        }
        catch(err){
            return ResponseModel.GetInvalidResponse(err);
        }
    }*/
    ActorService.UpdateActor = function (req) {
        return __awaiter(this, void 0, void 0, function () {
            var options, actor, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        options = { new: true, upsert: true };
                        return [4 /*yield*/, actorModel_1.actorModel.findByIdAndUpdate(req.params.actorId, req.body, options)];
                    case 1:
                        actor = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse(actor)];
                    case 2:
                        err_3 = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_3)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ActorService.DeleteActor = function (req) {
        return __awaiter(this, void 0, void 0, function () {
            var err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, actorModel_1.actorModel.findByIdAndDelete(req.params.actorId)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse("deleted")];
                    case 2:
                        err_4 = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_4)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return ActorService;
}());
exports.ActorService = ActorService;
