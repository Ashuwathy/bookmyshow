"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var showModel_1 = require("../models/showModel");
var responseModel_1 = require("../dataModels/responseModel");
var ShowService = /** @class */ (function () {
    function ShowService() {
    }
    //functions
    ShowService.CreateShow = function (req) {
        return __awaiter(this, void 0, void 0, function () {
            var newShow, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        newShow = new showModel_1.showModel(req.body);
                        return [4 /*yield*/, newShow.save()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse(newShow)];
                    case 2:
                        err_1 = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_1)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ShowService.GetAllShows = function () {
        return __awaiter(this, void 0, void 0, function () {
            var allShows, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, showModel_1.showModel.find({}).populate([
                                {
                                    path: "movie", select: "name language duration cast",
                                    populate: { path: "cast", select: "name" }
                                },
                                {
                                    path: "theatre", select: "name address"
                                }
                            ]).exec()];
                    case 1:
                        allShows = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse(allShows)];
                    case 2:
                        err_2 = _a.sent();
                        console.log(err_2);
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_2)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ShowService.GetShowsByTime = function (req) {
        return __awaiter(this, void 0, void 0, function () {
            var ShowsByTime, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, showModel_1.showModel.find({ timing: { start: parseInt(req.query.start), end: parseInt(req.query.end) } }).exec()];
                    case 1:
                        ShowsByTime = _a.sent();
                        if (ShowsByTime) {
                            return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse(ShowsByTime)];
                        }
                        else {
                            return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse("shows not available @ " + req.query.start + req.query.end)];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        err_3 = _a.sent();
                        console.log(err_3);
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_3)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ShowService.GetShowsByTheatre = function (req) {
        return __awaiter(this, void 0, void 0, function () {
            var allShows, ShowsByTheatre_1, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, showModel_1.showModel.find({}).populate([
                                {
                                    path: "movie", select: "name language duration cast",
                                    populate: { path: "cast", select: "name" }
                                },
                                {
                                    path: "theatre", select: "name address"
                                }
                            ]).exec()];
                    case 1:
                        allShows = _a.sent();
                        if (allShows.length > 0) {
                            ShowsByTheatre_1 = [];
                            ShowsByTheatre_1 = allShows.filter(function (item) {
                                if (item.theatre.name == req.query.theatreName) {
                                    ShowsByTheatre_1.push(item);
                                }
                            });
                            return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse(ShowsByTheatre_1)];
                        }
                        else {
                            return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse("shows not available in " + req.params.theatreId)];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        err_4 = _a.sent();
                        console.log(err_4);
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_4)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ShowService.GetShowsByDate = function (req) {
        return __awaiter(this, void 0, void 0, function () {
            var ShowsByDate, err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, showModel_1.showModel.find({ showDate: req.query.showDate }).populate([
                                {
                                    path: "movie", select: "name language duration cast",
                                    populate: { path: "cast", select: "name" }
                                },
                                {
                                    path: "theatre", select: "name address"
                                }
                            ]).exec()];
                    case 1:
                        ShowsByDate = _a.sent();
                        if (ShowsByDate) {
                            return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse(ShowsByDate)];
                        }
                        else {
                            return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse("shows not available @ " + req.query.showDate)];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        err_5 = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_5)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ShowService.GetShowsByMovie = function (req) {
        return __awaiter(this, void 0, void 0, function () {
            var allShows, ShowsByMovie_1, err_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, showModel_1.showModel.find({}).populate([
                                {
                                    path: "movie", select: "name language duration cast",
                                    populate: { path: "cast", select: "name" }
                                },
                                {
                                    path: "theatre", select: "name address"
                                }
                            ]).exec()];
                    case 1:
                        allShows = _a.sent();
                        if (allShows.length > 0) {
                            ShowsByMovie_1 = [];
                            ShowsByMovie_1 = allShows.filter(function (item) {
                                if (item.movie.name == req.query.movieName) {
                                    ShowsByMovie_1.push(item);
                                }
                            });
                            return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse(ShowsByMovie_1)];
                        }
                        else {
                            return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse("shows not available in " + req.params.theatreId)];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        err_6 = _a.sent();
                        console.log(err_6);
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_6)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ShowService.UpdateShow = function (req) {
        return __awaiter(this, void 0, void 0, function () {
            var options, show, err_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        options = { new: true, upsert: true };
                        return [4 /*yield*/, showModel_1.showModel.findByIdAndUpdate(req.params.showId, req.body, options)];
                    case 1:
                        show = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse(show)];
                    case 2:
                        err_7 = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_7)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ShowService.DeleteShow = function (req) {
        return __awaiter(this, void 0, void 0, function () {
            var err_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, showModel_1.showModel.findByIdAndDelete(req.params.showId)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetValidResponse("deleted")];
                    case 2:
                        err_8 = _a.sent();
                        return [2 /*return*/, responseModel_1.ResponseModel.GetInvalidResponse(err_8)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return ShowService;
}());
exports.ShowService = ShowService;
