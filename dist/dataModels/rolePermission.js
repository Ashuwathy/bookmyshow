"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var permission;
(function (permission) {
    permission[permission["AddGetUpdateDeleteMovie"] = 10] = "AddGetUpdateDeleteMovie";
    permission[permission["AddGetUpdateDeleteTheatre"] = 20] = "AddGetUpdateDeleteTheatre";
    permission[permission["AddGetUpdateDeleteActor"] = 30] = "AddGetUpdateDeleteActor";
    permission[permission["GetShow"] = 40] = "GetShow";
    permission[permission["AddUpdateDeleteShow"] = 41] = "AddUpdateDeleteShow";
    permission[permission["CreateDeleteBooking"] = 50] = "CreateDeleteBooking";
    permission[permission["GetBooking"] = 51] = "GetBooking";
    permission[permission["GetUser"] = 60] = "GetUser";
    permission[permission["All"] = 1000] = "All";
})(permission = exports.permission || (exports.permission = {}));
exports.rolePermission = {
    "user": [permission.GetShow, permission.GetShow, permission.GetUser],
    "admin": [permission.All]
};
