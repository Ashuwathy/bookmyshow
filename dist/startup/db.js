"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var DB = /** @class */ (function () {
    //functions
    function DB() {
    }
    DB.ConnectMongoDb = function () {
        mongoose_1.default.connect(this.mongoUrl) //method to connect to database
            .then(function () {
            console.log("ShowDB connected");
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    //properties
    DB.mongoUrl = "mongodb://localhost:27017/ShowDB"; //mongodb server url and mydb is database name and it is connectivity string
    return DB;
}());
exports.DB = DB;
