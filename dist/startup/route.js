"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var movieRoutes_1 = require("../routes/movieRoutes");
var actorRoutes_1 = require("../routes/actorRoutes");
var bookingRoutes_1 = require("../routes/bookingRoutes");
var showRoutes_1 = require("../routes/showRoutes");
var theatreRoutes_1 = require("../routes/theatreRoutes");
var userRoutes_1 = require("../routes/userRoutes");
var userController_1 = require("../controller/userController");
var Route = /** @class */ (function () {
    //functions
    function Route() {
    }
    Route.configRoutes = function (app) {
        app.post("/api/login", userController_1.UserController.Login);
        app.post("/api/register", userController_1.UserController.Register);
        //app.use(AuthenticateService.Authenticate);
        app.use("/api/movie", movieRoutes_1.movieRoutes); //end point,action to be performed
        app.use("/api/actor", actorRoutes_1.actorRoutes);
        app.use("/api/booking", bookingRoutes_1.bookingRoutes);
        app.use("/api/show", showRoutes_1.showRoutes);
        app.use("/api/theatre", theatreRoutes_1.theatreRoutes);
        app.use("/api/user", userRoutes_1.userRoutes);
        /*
        //mongoose methods
        app.post("/api/inventory/insertOne",MongooseHelper.InsertOne);
        app.post("/api/inventory/insertMany",MongooseHelper.InsertMany);
        app.get("/api/inventory/getOneItem",MongooseHelper.FindItem);
        app.get("/api/inventory/getAllItem",MongooseHelper.FindAllItem);
        app.put("/api/inventory/updateItem",MongooseHelper.Update);
        app.put("/api/inventory/updateMany",MongooseHelper.UpdateMany);

        //http methods
        app.get("/api/test/:testid",(req:express.Request)=>{
            {
                console.log(req.query);
                console.log(req.params);
                console.log(req.body);
            }

        });
        app.post("/api/test/:testid",(req:express.Request)=>{
            {
                console.log(req.query);
                console.log(req.params);
                console.log(req.body);
            }

        });
        app.put("/api/test/:testid",(req:express.Request)=>{
            {
                console.log(req.query);
                console.log(req.params);
                console.log(req.body);
            }

        });
        app.delete("/api/test/:testid",(req:express.Request)=>{
            {
                console.log(req.query);
                console.log(req.params);
                console.log(req.body);
            }

        });*/
    };
    return Route;
}());
exports.Route = Route;
