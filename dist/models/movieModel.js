"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var movieSchema = new mongoose_1.default.Schema({
    name: { type: String, required: true, unique: true },
    language: { type: String, enum: ['hindi', 'tamil', 'english'], default: 'tamil' },
    duration: { type: Number },
    cast: [{ type: mongoose_1.default.Schema.Types.ObjectId, ref: "actor" }],
    active: { type: Boolean, default: false },
    createdDate: { type: Date, default: Date.now },
    updatedDate: { type: Date, default: Date.now },
});
exports.movieModel = mongoose_1.default.model("movie", movieSchema);
