"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var actorSchema = new mongoose_1.default.Schema({
    name: { type: String, required: true, unique: true },
    gender: { type: String, enum: ['male', 'female'] },
});
exports.actorModel = mongoose_1.default.model("actor", actorSchema);
