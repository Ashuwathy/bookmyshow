"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var inventorySchema = new mongoose_1.default.Schema({
    item: { type: String, required: true },
    qty: {
        type: Number,
        validate: {
            validator: function (v) {
                return v > 10;
            },
            message: "quantity should be greater than 10"
        }
    },
    size: {
        h: Number,
        w: Number,
        uom: String
    },
    tags: [String],
    status: String
});
exports.inventoryModel = mongoose_1.default.model("inventory", inventorySchema);
