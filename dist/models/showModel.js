"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var showSchema = new mongoose_1.default.Schema({
    showDate: { type: Date, default: Date.now },
    timing: { start: Number, end: Number },
    movie: [{ type: mongoose_1.default.Schema.Types.ObjectId, ref: "movie" }, { type: mongoose_1.default.Schema.Types.ObjectId, ref: "theatre" }],
    price: { type: Number, required: true },
    seats: { type: Number, default: 25 },
    theatre: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "theatre" }
});
exports.showModel = mongoose_1.default.model("show", showSchema);
