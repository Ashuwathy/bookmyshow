"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var addressSchema_1 = require("./common/addressSchema");
var theatreSchema = new mongoose_1.default.Schema({
    name: { type: String, required: true },
    address: addressSchema_1.addressSchema
});
exports.theatreModel = mongoose_1.default.model("theatre", theatreSchema);
