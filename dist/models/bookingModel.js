"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var bookingSchema = new mongoose_1.default.Schema({
    show: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "show" },
    user: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "user" },
    date: { type: Date, default: Date.now },
    status: { type: String, enum: ['pending', 'completed', 'failed'], default: 'completed' },
});
exports.bookingModel = mongoose_1.default.model("booking", bookingSchema); //register collection in database
